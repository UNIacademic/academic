import React, { Component } from 'react';
import Paper from 'material-ui/Paper';
import MenuItem from 'material-ui/MenuItem';
import RaisedButton from 'material-ui/RaisedButton';
import { Link } from 'react-router-dom'
import Divider from 'material-ui/Divider';
import GooglePicker from 'react-google-picker';

import './page.css';

const CLIENT_ID = '440387728341-170312p7i9k2f5hpe8t73ek8cv6ro5hm.apps.googleusercontent.com';
const DEVELOPER_KEY = 'AIzaSyBGHKqGWcsvDx7sp9m7WIzqEwCrFC-jrCA';
const SCOPE = ['https://www.googleapis.com/auth/drive.readonly',
               'https://www.googleapis.com/auth/drive',
               'https://www.googleapis.com/auth/drive.file',
               'https://www.googleapis.com/auth/drive.metadata',
               'https://www.googleapis.com/auth/drive.appfolder',
               'https://www.googleapis.com/auth/drive.scripts'];


export default class Page extends Component {

    constructor(props) {
        super(props);
        this.callDatePicker = this.callDatePicker.bind(this);
    }

    callDatePicker() {
    return <Paper zDepth={2} style={{marginTop: '2em', width: '100%'}}>
        
    </Paper>
    }

    render() {

        return(
        <div className='page'>
            <div style={{width: '15%', zIndex: 1}}>
                <Paper className='sidebar'>
                <div style={{padding: '1em'}}>
                    <img width = '100%' src='./../../../assets/img/logo_white.png'/>
                </div>
                <Divider style={{height: '3px'}}/>
                <div style={{paddingTop: '3em'}}>
                    <MenuItem containerElement={<Link to="/atividade" />} className='opMenu'> Atividades </MenuItem>
                    <MenuItem containerElement={<Link to="/pessoa" />} className='opMenu'>Pessoas</MenuItem>
                        <GooglePicker clientId={CLIENT_ID}
                                        developerKey={DEVELOPER_KEY}
                                        scope={SCOPE}
                                        style={{display: 'none'}}
                                        onChange={data => console.log('on change:', data)}
                                        multiselect={true}
                                        navHidden={false}
                                        authImmediate={false}
                                        id='button-google'
                                        viewId={'FOLDERS'}
                                        createPicker={ (google, oauthToken) => {
                                            const googleViewId = google.picker.ViewId.FOLDERS;
                                            const docsView = new google.picker.DocsView(googleViewId)
                                                .setIncludeFolders(true)
                                                .setSelectFolderEnabled(true)
                                                .setMimeTypes('application/vnd.google-apps.folder');
                                            
                                            const uploadView = new google.picker.DocsUploadView();
                                            const picker = new google.picker.PickerBuilder()
                                                .addView(docsView)
                                                .addView(google.picker.ViewId.DOCUMENTS)
                                                .addView(google.picker.ViewId.DOCS)
                                                .addView(google.picker.ViewId.PDFS)
                                                .addView(uploadView)
                                                .enableFeature(google.picker.Feature.MULTISELECT_ENABLED)
                                                .setOAuthToken(oauthToken)
                                                .setDeveloperKey(DEVELOPER_KEY)
                                                .setCallback(()=>{
                                                });

                                            picker.build().setVisible(true);
                                        }}>
                            <MenuItem className='opMenu'> Documentos </MenuItem>
                        </GooglePicker>
                </div>
                </Paper>
            </div>
            
            <div className='appBar'>
                <h3 className='title'>  {this.props.title} </h3>
            </div>
            <div className='content'>    
                {this.props.children}
                <div className="google"></div>
            </div>
        </div>
        )
    }
}